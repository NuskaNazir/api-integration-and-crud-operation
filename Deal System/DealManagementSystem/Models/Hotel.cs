public class Hotel
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public int Rating { get; set; }
    public List<string> ImageUrls { get; set; } = new List<string>();
}
