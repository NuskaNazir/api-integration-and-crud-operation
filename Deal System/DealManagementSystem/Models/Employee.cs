namespace DealManagementSystem.Models;

public class Employee
{
    public DateTimeOffset createdAt { get; set; } 
    public string? Name { get; set; }
    public string? Avatar { get; set; }
    public int Id { get; set; }
}